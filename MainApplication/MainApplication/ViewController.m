//
//  ViewController.m
//  MainApplication
//
//  Created by Asad Ali on 18/09/2014.
//  Copyright (c) 2014 DevBatch. All rights reserved.
//

#import "ViewController.h"

#define kChild1URLScheme @"com.devbatch.tmp.child1"
#define kChild2URLScheme @"com.devbatch.tmp.child2"
#define kMainURLScheme @"com.devbatch.tmp.main"

#define kDataToSend [@"I'm getting data which is shared with me through custom URL" stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]

#define kQuestionToAsk [@"Q: Are you male?" stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Buttons Action

- (IBAction)btnSendDataToChild1Clicked:(id)sender
{
    NSString *urlString = [NSString stringWithFormat:@"%@://%@", kChild1URLScheme, kDataToSend];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)btnSendDataToChild2Clicked:(id)sender
{
    NSString *urlString = [NSString stringWithFormat:@"%@://%@", kChild2URLScheme, kDataToSend];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)btnSendImageToChild1Clicked:(id)sender
{
}

- (IBAction)btnSendImageToChild2Clicked:(id)sender
{
}

- (IBAction)btnAskQuestionFromChild1Clicked:(id)sender
{
    NSString *urlString = [NSString stringWithFormat:@"%@://%@", kChild1URLScheme, kQuestionToAsk];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)btnAskQuestionFromChild2Clicked:(id)sender
{
    NSString *urlString = [NSString stringWithFormat:@"%@://%@", kChild2URLScheme, kQuestionToAsk];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


@end
