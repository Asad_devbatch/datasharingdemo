//
//  AppDelegate.h
//  ChildApplication1
//
//  Created by Asad Ali on 18/09/2014.
//  Copyright (c) 2014 DevBatch. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kMainURLScheme @"com.devbatch.tmp.main"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
